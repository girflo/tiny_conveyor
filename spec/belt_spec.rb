# frozen_string_literal: true

require 'spec_helper'

RSpec.describe TinyConveyor::Belt do
  let(:conveyor) { TinyConveyor::Belt.new }

  describe '.add_parcel' do
    let(:action) { proc { puts 'Run in a separated thread' } }
    subject { conveyor.add_parcel('name', 'description', action) }

    it 'adds the parcel to the belt' do
      expect { subject }.to change { conveyor.parcels.count }.from(0).to(1)
    end
  end

  describe '.remove_parcel_by_uuid' do
    let(:action) { proc { puts 'Run in a separated thread' } }
    before { conveyor.add_parcel('name', 'description', action) }
    let(:uuid) { conveyor.parcels.first.uuid }
    subject { conveyor.remove_parcel_by_uuid(uuid) }

    it 'removes the parcel from the belt' do
      expect { subject }.to change { conveyor.parcels.count }.from(1).to(0)
    end
  end

  describe '.start_belt' do
    let(:action) { proc { puts 'Run in a separated thread' } }
    before { 3.times { conveyor.add_parcel('name', 'description', action) } }
    subject { conveyor.start_belt }

    it 'executes the belt' do
      subject
      expect(conveyor.parcels).to be_empty
    end
  end
end
