# frozen_string_literal: true

require 'English'

Gem::Specification.new do |s|
  s.name          = 'tiny_conveyor'
  s.version       = '0.2.0'
  s.required_ruby_version = '>= 3.1.1 '
  s.summary       = 'Tiny wrapper aroung async to create a queue of tasks'
  s.authors       = ['Flo Girardo']
  s.email         = 'florian@barbrousse.net'
  s.files = `git ls-files`.split($RS).reject do |file|
    file =~ %r{^spec/}
  end
  s.require_paths = ['lib']
  s.homepage      =
    'https://gitlab.com/girflo/tiny_conveyor'
  s.license       = 'MIT'
  s.add_dependency 'async', '~> 1.2'
  s.add_development_dependency('rspec', '~> 3')
  s.add_development_dependency('rubocop', '~> 1.22')
  s.add_development_dependency('yard', '~> 0.9')
end
