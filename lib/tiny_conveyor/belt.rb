# frozen_string_literal: true

module TinyConveyor
  # Thread manager, allow creation and execution of parcels
  class Belt
    require 'async'
    attr_reader :parcels

    # @return [Belt] newly created belt
    def initialize
      super()
      @parcels = []
      @threads = []
      @state = :waiting
    end

    # @param name [string] name of the task
    # @param description [string] small description of the task
    # @param action [Proc] action to run in separate thread
    # @return [Array<Parcel>] pending parcels
    def add_parcel(name, description, task)
      parcel = Parcel.new(name, description, task)
      @parcels << parcel
    end

    # @param uuid [string] uuid of the parcel to remove
    # @return [Array<Parcel>] pending parcels
    def remove_parcel_by_uuid(uuid)
      parcel = @parcels.find { |p| p.uuid == uuid }
      return @parcels if parcel.nil?

      remove_parcel(parcel)
    end

    def start_belt
      Async::Reactor.run do |task|
        task.async do
          run_belt_entirely
        end
      end
    end

    def run_belt_entirely
      execute until @parcels.empty?
    end

    # @return [boolean] true if the belt is running, false otherwhise
    def running?
      @state == :running
    end

    def remove_parcel(parcel)
      new_parcels_array = @parcels.reject { |p| p == parcel }
      @parcels = new_parcels_array
    end

    private

    def execute
      return true if running?

      tasks = @parcels.clone
      @state = :running
      until tasks.empty?
        current_parcel = tasks.shift
        current_parcel&.execute
        @parcels.shift
      end
      @state = :waiting
    end
  end
end
