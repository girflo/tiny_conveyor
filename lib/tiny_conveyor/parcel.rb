# frozen_string_literal: true

require 'securerandom'
module TinyConveyor
  # Encapsulate an action to run in a separated thread
  class Parcel
    attr_reader :name, :description, :state, :uuid

    # @param name [string] name of the task
    # @param description [string] small description of the task
    # @param action [Proc] action to run in separate thread
    # @return [Parcel] newly created parcel
    def initialize(name, description, action)
      @name = name
      @description = description
      @action = action
      @state = :pending
      @uuid = SecureRandom.uuid
    end

    def execute
      @state = :running
      @action.call
    end

    # @return [boolean] true if the parcel is executed, false otherwhise
    def running?
      @state == :running
    end
  end
end
