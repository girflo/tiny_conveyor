# frozen_string_literal: true

require 'tiny_conveyor/belt'
require 'tiny_conveyor/parcel'

# main file for Tiny Conveyor gem
module TinyConveyor
  class << self
    # @return [Belt] newly created belt
    def new
      Belt.new
    end
  end
end
